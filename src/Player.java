import java.util.Random;

public class Player
{
    private String name;
    private boolean gender;
    private int strength;
    private String strengthRank;
    private int intelligence;
    private String intelligenceRank;
    private int agility;
    private String agilityRank;
    private Classes classes;

    public void setAttribute()
    {
        Random random = new Random();
        int maxStr, minStr, maxInt, minInt, maxAgil, minAgil;
        switch (classes)
        {
            case WARRIOR ->
            {
                maxStr = 10; minStr = 3; maxInt = 4; minInt = 1; maxAgil = 5; minAgil = 2;
                strength = random.nextInt(maxStr - minStr) + minStr;
                intelligence = random.nextInt(maxInt - minInt) + minInt;
                agility = random.nextInt(maxAgil - minAgil) + minAgil;
            }
            case MAGICIAN ->
            {
                maxStr = 3; minStr = 1; maxInt = 10; minInt = 3; maxAgil = 3; minAgil = 1;
                strength = random.nextInt(maxStr - minStr) + minStr;
                intelligence = random.nextInt(maxInt - minInt) + minInt;
                agility = random.nextInt(maxAgil - minAgil) + minAgil;
            }
            case ASSASSIN ->
            {
                maxStr = 4; minStr = 1; maxInt = 4; minInt = 2; maxAgil = 10; minAgil = 5;
                strength = random.nextInt(maxStr - minStr) + minStr;
                intelligence = random.nextInt(maxInt - minInt) + minInt;
                agility = random.nextInt(maxAgil - minAgil) + minAgil;
            }
        }

        setRank();
    }

    public void setRank()
    {
        if(strength <= 3)
            strengthRank = "B";
        else if(strength <= 6)
            strengthRank = "A";
        else if(strength <= 11)
            strengthRank = "S";

        if(intelligence <= 3)
            intelligenceRank = "B";
        else if(intelligence <= 6)
            intelligenceRank = "A";
        else if(intelligence <= 11)
            intelligenceRank = "S";

        if(agility <= 3)
            agilityRank = "B";
        else if(agility <= 6)
            agilityRank = "A";
        else if(agility <= 11)
            agilityRank = "S";
    }

    public void getAllAttribute()
    {
        System.out.println("=============================================");
        System.out.println("               YOUR ATTRIBUTE");
        System.out.println("=============================================");
        System.out.println("Name\t\t: " + name);
        System.out.println("Gender\t\t: " + (gender ? "Male" : "Female"));
        System.out.println("Class\t\t: " + classes);
        System.out.println("Strength\t: " + strength + " (" + strengthRank + ")");
        System.out.println("Intelligence: " + intelligence + " (" + intelligenceRank + ")");
        System.out.println("Agility\t\t: " + agility + " (" + agilityRank + ")");
        System.out.println("=============================================");
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean isGender() {
        return gender;
    }
    public void setGender(boolean gender) {
        this.gender = gender;
    }
    public int getStrength() {
        return strength;
    }
    public int getIntelligence() {
        return intelligence;
    }
    public int getAgility() {
        return agility;
    }
    public Classes getClasses() {
        return classes;
    }
    public void setClasses(Classes classes) {
        this.classes = classes;
    }
}
