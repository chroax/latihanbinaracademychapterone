import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TitleList
{
    Scanner input = new Scanner(System.in);;
    List <Player> players = new ArrayList<>();

    TitleList()
    {
        System.out.println("\n===============================");
        System.out.println("  Hello Welcome to My Program");
        System.out.println("===============================\n");
        System.out.println();
    }

    public void mainMenu()
    {
        System.out.println("""
                        --------------------------------------
                         Pilih apa yang akan kamu lakukan
                        --------------------------------------
                         1. Menambah player
                         2. Melihat semua data player
                         3. Melihat data player
                         0. Exit
                        """);
        System.out.print("Input : ");
        int option = input.nextInt();
        input.nextLine();
        System.out.println();
        chooseOption(option);
    }

    public void chooseOption(int id)
    {
        switch (id)
        {
            case 1 -> addPlayer();
            case 2 -> seeAllPlayer();
            case 3 -> seePlayerData();
            default -> System.exit(0);
        }
    }

    public void addPlayer()
    {
        Player player = new Player();
        String name;
        boolean gender = true;
        int optGender;
        int optClasses;

        System.out.print("Enter your name: ");
        name = input.nextLine();

        System.out.println("\n=====================");
        System.out.println("1. Male");
        System.out.println("2. Female");
        System.out.print("Enter your gender: ");
        optGender = input.nextInt();
        if(optGender == 1)
            gender = true;
        else if(optGender == 2)
            gender = false;

        System.out.println("\n=====================");
        System.out.println("1. WARRIOR");
        System.out.println("2. MAGICIAN");
        System.out.println("3. ASSASSIN");
        System.out.print("Enter your classes: ");
        optClasses = input.nextInt();
        if(optClasses == 1)
            player.setClasses(Classes.WARRIOR);
        else if(optClasses == 2)
            player.setClasses(Classes.MAGICIAN);
        else if(optClasses == 3)
            player.setClasses(Classes.ASSASSIN);

        player.setAttribute();
        player.setName(name);
        player.setGender(gender);
        System.out.println();
        player.getAllAttribute();
        System.out.println();
        this.players.add(player);

        mainMenu();
    }

    public void seeAllPlayer()
    {
        System.out.println("=================================================================================");
        System.out.println("ID\t\tName\t\tGender\t\tClass\t\tStr\t\tInt\t\tAgi");

        int id = 1;
        for (Player player : this.players)
        {
            System.out.println(id + "\t\t" + player.getName() +
                    "\t\t" + (player.isGender() ? "Male" : "Female") +
                    "\t\t" + player.getClasses() +
                    "\t\t" + player.getStrength() +
                    "\t\t" + player.getIntelligence() +
                    "\t\t" + player.getAgility());
            id++;
        }
        System.out.println("=================================================================================\n");

        mainMenu();
    }

    public void seePlayerData(){
        System.out.print("Input ID : ");
        int id = input.nextInt();
        Player player = players.get(id);
        player.getAllAttribute();
    }

    public static void main(String[] args) {
        TitleList playerList = new TitleList();
        playerList.mainMenu();
    }
}
